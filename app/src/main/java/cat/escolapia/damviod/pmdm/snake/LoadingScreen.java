package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", PixmapFormat.ARGB4444);
        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("ready.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.headUp = g.newPixmap("headup.png", PixmapFormat.ARGB4444);
        Assets.headLeft = g.newPixmap("headleft.png", PixmapFormat.ARGB4444);
        Assets.headDown = g.newPixmap("headdown.png", PixmapFormat.ARGB4444);
        Assets.headRight = g.newPixmap("headright.png", PixmapFormat.ARGB4444);
        Assets.tail = g.newPixmap("tail.png", PixmapFormat.ARGB4444);
        Assets.diamond = g.newPixmap("diamond.png", PixmapFormat.ARGB4444);
        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.eat = game.getAudio().newSound("eat.ogg");
        Assets.xoc = game.getAudio().newSound("bitten.ogg");
        Settings.load(game.getFileIO());
        Assets.Credits = g.newPixmap("Credits.png", PixmapFormat.ARGB4444);
        Assets.jose = g.newPixmap("jose.png", PixmapFormat.ARGB4444);
        Assets.background2 = g.newPixmap("background2.png", PixmapFormat.RGB565);
        Assets.background4 = g.newPixmap("background4.png", PixmapFormat.RGB565);
        Assets.background5 = g.newPixmap("background5.png", PixmapFormat.RGB565);
        Assets.wall = g.newPixmap("wall.png", PixmapFormat.ARGB4444);
        Assets.music1 = game.getAudio().newMusic("musicgame.mp3");

        game.setScreen(new MainMenuScreen(game));

    }
    
    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}

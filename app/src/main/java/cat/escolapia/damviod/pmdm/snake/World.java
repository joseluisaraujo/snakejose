package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;

    public Snake snake;
    public Diamond diamond;
    public Diamond diamond2;
    public Wall wall;
    public boolean gameOver = false;;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
        placeDiamond(1);
        placeDiamond(2);
        wall = new Wall(WORLD_WIDTH/2 -2,WORLD_HEIGHT/2 -2, Wall.TYPE_1);
    }

    private void placeDiamond(int n) {
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
        if(n==1) diamond = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        else diamond2 = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
    }

    public void update(float deltaTime) {
        if (gameOver) return;
        
        tickTime += deltaTime;

        tickTime+=(0.01f *score/10);
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance(score);
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            if(snake.checkFeed(diamond))
            {
                snake.allarga();
                placeDiamond(1);
                score = score + SCORE_INCREMENT;

            }
            if(snake.checkFeed(diamond2))
            {
                snake.allarga();
                placeDiamond(2);
                score = score + SCORE_INCREMENT;

            }
            if(snake.checkWall(wall))
            {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
        }
    }
}

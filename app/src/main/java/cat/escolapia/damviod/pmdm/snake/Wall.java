package cat.escolapia.damviod.pmdm.snake;

/**
 * Created by joseluis.araujo on 07/12/2016.
 */
public class Wall {
    public static final int TYPE_1 = 0;
    public int x, y;
    public int type;

    public Wall(int x, int y, int type) {
        this.x = x;
        this.y = y;

    }

}
